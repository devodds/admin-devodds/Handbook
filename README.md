# DevOdds Manual

Este manual de trabajo te introduce y explica todo lo necesario para formar parte del equipo de [**DevOdds**](https://devodds.com/). Somos una empresa pequeña pero nos propusimos ser transparentes desde el principio, entonces acá pretende estar todo lo que hacemos y como lo hacemos.

> Tabla de Contenido generada con [DocToc](https://github.com/thlorenz/doctoc)

- [Empresa](#empresa)

## Empresa

**DevOdds** es una empresa creada para desarrollar *software* de calidad, nos gusta trabajar **bien** y cada día mejorar entre todos como lo hacemos. Para cumplir poder cumplir esto queremos que todos inviertan tiempo en buscar nuevas tecnologias y herramientas que ayuden en esto.
